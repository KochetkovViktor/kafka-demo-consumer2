package ru.vek.kafka.example.consumer2.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import ru.vek.kafka.example.consumer2.dto.PuppyDto;
import ru.vek.kafka.example.consumer2.service.PuppyService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaEventListener {

    private final PuppyService service;

    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    @KafkaListener(topics = "${kafka.kennel.topic.name}", groupId = "${kafka.group.id}")
    public void listenKafka(String message) {
        service.addNewPuppyToWishList(convertMessageToEntity(message));
    }

    private PuppyDto convertMessageToEntity(String message) {
        PuppyDto dto = new PuppyDto();
        try {
            JSONObject json = new JSONObject(message);
            dto.setName(json.get("name").toString());
            dto.setBreed(json.get("breed").toString());
            dto.setVetNumber(json.get("vetNumber").toString());
            dto.setRegistrationDateTime(
                    LocalDateTime.parse(json.get("registrationDateTime").toString(), dateTimeFormatter)
            );
            return dto;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
