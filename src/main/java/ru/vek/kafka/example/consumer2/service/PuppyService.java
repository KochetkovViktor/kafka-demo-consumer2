package ru.vek.kafka.example.consumer2.service;

import ru.vek.kafka.example.consumer2.dto.PuppyDto;

import java.util.List;

public interface PuppyService {

    void addNewPuppyToWishList(PuppyDto dto);

    List<PuppyDto> getAllPuppies();
}
