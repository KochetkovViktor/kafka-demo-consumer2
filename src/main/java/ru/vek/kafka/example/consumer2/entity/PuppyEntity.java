package ru.vek.kafka.example.consumer2.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "puppy")
@SequenceGenerator(name = "puppySequence", sequenceName = "PUPPY_SEQ", allocationSize = 1)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PuppyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "puppySequence")
    Long id;

    @Column
    String name;

    @Column
    String breed;

    @Column(name = "vet_number")
    String vetNumber;

    @Column(name = "registration_date_time")
    LocalDateTime registrationDateTime;

}
