package ru.vek.kafka.example.consumer2.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PuppyDto {

    String name;

    String breed;

    LocalDateTime registrationDateTime;

    String vetNumber;
}
