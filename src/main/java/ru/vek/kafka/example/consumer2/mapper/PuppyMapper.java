package ru.vek.kafka.example.consumer2.mapper;

import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import ru.vek.kafka.example.consumer2.dto.PuppyDto;
import ru.vek.kafka.example.consumer2.entity.PuppyEntity;

import java.util.List;
import java.util.stream.Collectors;


@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        collectionMappingStrategy = CollectionMappingStrategy.TARGET_IMMUTABLE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        componentModel = "spring")
public interface PuppyMapper {

    PuppyDto convertToDto(PuppyEntity source);

    default List<PuppyDto> convertToDtoList(List<PuppyEntity> source) {
        return source.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    PuppyEntity convertToEntity(PuppyDto source);
}
