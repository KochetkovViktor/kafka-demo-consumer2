package ru.vek.kafka.example.consumer2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vek.kafka.example.consumer2.entity.PuppyEntity;


public interface PuppyRepository extends JpaRepository<PuppyEntity, Long> {
}
