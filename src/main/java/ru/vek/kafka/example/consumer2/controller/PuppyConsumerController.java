package ru.vek.kafka.example.consumer2.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.vek.kafka.example.consumer2.dto.PuppyDto;
import ru.vek.kafka.example.consumer2.service.PuppyService;

import java.util.List;

@RestController
@Api(value = "API получения информации из питомника",
        protocols = "http,https"
)
@RequestMapping("${urls.app.consumer.root}")
@RequiredArgsConstructor
@Slf4j
public class PuppyConsumerController {

    private final PuppyService service;

    @ApiOperation(value = "Наличие щенков в питомнике")
    @GetMapping
    public ResponseEntity<List<PuppyDto>> getAllPappies() {
        var responseDto = service.getAllPuppies();
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }
}
